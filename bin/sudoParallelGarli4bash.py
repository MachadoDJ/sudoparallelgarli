#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#sudoParallelGarli_bash_v10.py

"""
Author: Denis Jacob Machado
Homepage: https://about.me/machadodj
Email: denisjacobmachado[at]gmail.com

LICENSE

This program may be freely used, modified, and shared under the GNU General
Public License version 3.0 (GPL-3.0, http://opensource.org/licenses/GPL-3.0).
"""

"""
DESCRIPTION

Sudo Parallelization of Garli - v1.0

DEPENDENCIES

Garli v2.01
Python 3
Packages: argparse,sys,re,os,glob
"""

"""
VERSION

170528
"""

# Required Python libraries
import argparse,sys,re,os,glob,subprocess

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-c","--configuration",help="Path to configuration file (defaulf = garli.conf)",type=str,default="garli.conf",required=True)
parser.add_argument("-g","--garli",help="Path to garli executable (defaulf = garli)",type=str,default="garli",required=True)
parser.add_argument("-p","--processors",help="Maximum number of CPUs to use (default = 1)",type=int,default=1,required=False)
parser.add_argument("-r","--replicas",help="Specify a valid number of replicas greater than 1 (default = 1)",type=int,default=1,required=False)
parser.add_argument("-s","--select",help="Select best tree and quit (default = off)",action="store_true",default=False,required=False)
parser.add_argument("-S","--Start",help="Specify replicate start number (default = 1)",type=int,default=1,required=False)
parser.add_argument("-V","--version",help="print version number and exit",action="store_true",default=False)
args=parser.parse_args()

###########
# VERSION #
###########

if(args.version):
	sys.stdout.write("version 2017.05.28\n")
	sys.stdout.flush()
	exit()

####################
# SELECT BEST TREE #
####################

if args.select:
	FILE_EXT="*best.tre"
	BEST_LIKE=[]
	REPLICA=[]
	for LOG in glob.iglob(os.path.join(FILE_EXT)):
		NO=re.compile("(\S+).best.tre").findall(LOG)
		NO=NO[0]
		REPLICA+=[NO]
		with open(LOG,"rU")as file:
			scores_dic={}
			text=file.read()
			scores=re.compile("!GarliScore\s+([^\]\s]+)").findall(text)
			if not scores:
				sys.stderr.write("! ERROR: could not find any instance of final optimization in {}\n".format(LOG))
				exit()
			else:
				for key in range(0,len(scores)):
					scores_dic[key]=float(scores[key])
				best_rep=sorted(scores_dic,key=scores_dic.get,reverse=True)[0]
				best_like=scores_dic[best_rep]
				BEST_LIKE+=[best_like]
			sys.stdout.write("Log file %s, best score %f\n"%(LOG,best_like))
	sys.stdout.write("The best likelihood score is %f\nThe best tree(s) will be copied to ./BEST_LIKE/\n"%(max(BEST_LIKE)))
	INDEX=[i for i,n in enumerate(BEST_LIKE) if n==max(BEST_LIKE)]
	os.system("mkdir BEST_LIKE/")
	for n in INDEX:
		command="cp "+REPLICA[n]+"* BEST_LIKE/"
		os.system(command)
	exit()

#################
# PARALLEL JOBS #
#################

# Check number of replicates
if(args.replicas<=1):
	sys.stderr.write("!Error: Please specify a valid number of replicas greater than 1\n")
	exit()
else:
	numberOfReps=args.replicas
	leadingZeros=len(str(numberOfReps)) # For file names

# Check start replica
if(args.Start<1):
	sys.stderr.write("!Error: Please specify a valid start replicate (1 or greater)\n")
	exit()
else:
	starter=args.Start

# Read configuration file
conf=args.configuration # You MUST have this file
try:
	with open(conf,"r")as confReadable:
		confStr=confReadable.read()
except IOError:
	sys.stderr.write("!Error: File {} was not found\n".format(conf))
	exit()
ofprefix=re.compile('ofprefix\s*=\s*(\S+)').findall(confStr)
if(len(ofprefix)>0):
	ofprefix=ofprefix[0]
else:
	sys.stderr.write("!Error: No ofprefix found in {}\n".format(conf))

# Prepare configuration files for parallel processing
configs=[]
for rep in range(starter,numberOfReps+starter):
	# Creat configuration files
	repStr=str(rep).rjust(leadingZeros,"0")
	prefix=ofprefix+repStr
	confNew=re.sub(ofprefix,prefix,confStr)
	confFileName="garli{}.conf".format(str(repStr))
	configs.append(confFileName)
	confFile=open(confFileName,"w")
	confFile.write(confNew)
	confFile.close()

while True:
	jobs=[]
	for config in configs[:args.processors]:
		jobs.append(subprocess.Popen("{} {} -b".format(args.garli,config),shell=True))
	for p in jobs:
		p.wait()
	configs=configs[args.processors:]
	if(len(configs)==0):
		break

exit()
