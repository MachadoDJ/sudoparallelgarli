#!/usr/bin/env python2.7
#-*- coding: utf-8 -*-

#sudoParallelGarli_v10.py

"""
Author: Denis Jacob Machado
Homepage: https://about.me/machadodj
Email: denisjacobmachado[at]gmail.com

LICENSE

This program may be freely used, modified, and shared under the GNU General
Public License version 3.0 (GPL-3.0, http://opensource.org/licenses/GPL-3.0).
"""

"""
DESCRIPTION

Sudo Parallelization of Garli - v1.0

EXAMPLE COMMAND LINE

$ python sudoGarliParallel.py [-r; -k; -s] [number of replicas; pbs prefix] [number to start with (default = 1)]

NOTES

Sudo parallelization of Garli: python sudoGarliParallel.py -r <number of replicas>
Kill all replicas: python sudoGarliParallel.py -k <pbs prefix>
Select best tree in directory: python sudoGarliParallel.py -s
Execute from the current project directory

DEPENDENCIES

Garli v2.01
Python 2.7
Packages: sys,re,os,glob
"""

"""
VERSION

170419
"""

import sys,re,os,glob # Required Python libraries

## Sudo parallelization

# print'There are %d arguments'%len(sys.argv)

if sys.argv[1]=='-r':

	# Read arguments
	if len(sys.argv)<3:
		print'Error 00: Specify a valid number of replicas greater than 1'
		exit()
	elif int(sys.argv[2])<=1:
		print'Error 00: Specify a valid number of replicas greater than 1'
		exit()
	else:numberOfReps=sys.argv[2]

	starter=0
	if len(sys.argv)>3:
		try:
			starter=int(sys.argv[3])
		except ValueError:pass

	# Read configuration file
	conf='garli.conf' # You MUST have this file
	try:
		with open(conf,'r')as confReadable:confStr=confReadable.read()
	except IOError:
		print'Error 01: File %s was not found'%conf
		exit()
	ofprefix=re.compile('ofprefix\s*=\s*(\S+)').findall(confStr)
	if len(ofprefix)>0:ofprefix=ofprefix[0]
	else:print'Error 03: No ofprefix found in %s'%conf

	# Read pbs file
	pbs='garli.pbs' # You MUST have this file
	try:
		with open(pbs,'r')as pbsReadable:pbsStr=pbsReadable.read()
	except IOError:
		print'Error 04: File %s was not found'%pbs
		exit()
	pbsName=re.compile('\s+\#PBS\s+-N\s+(\S+)').findall(pbsStr)
	if len(pbsName)==0:
		print'Error 05: Could not find #PBS -N parameter in %s'%pbs
		exit()
	else:pbsName=pbsName[0]

	leadingZeros=len(numberOfReps) # For file names

	# For each replica
	for rep in range(1+starter,int(numberOfReps)+1+starter):

		# Creat configuration files
		repStr=str(rep).rjust(leadingZeros,'0')
		prefix=ofprefix+repStr
		confNew=re.sub(ofprefix,prefix,confStr)
		confFileName='garli'+str(repStr)+'.conf'
		confFile=open(confFileName,'w')
		confFile.write(confNew)
		confFile.close()

		# Create pbs files
		pbsNameNew=pbsName+repStr
		pbsNew=re.sub(pbsName,pbsNameNew,pbsStr)
		pbsNew=re.sub('\.err',repStr+'.err',pbsNew)
		pbsNew=re.sub('\.out',repStr+'.out',pbsNew)
		pbsNew=re.sub('\.conf',repStr+'.conf',pbsNew)
		pbsFileName='garli'+str(repStr)+'.pbs'
		pbsFile=open(pbsFileName,'w')
		pbsFile=open(pbsFileName,'w')
		pbsFile.write(pbsNew)
		pbsFile.close()

		# Submit job
		os.system("qsub %s"%pbsFileName)

## Kill jobs
elif sys.argv[1]=='-k':

	# Read arguments
	if len(sys.argv)<3:
		print("Error 06: Specify the job name prefix (#PBS -N <job name> in garli.pbs)")
		exit()
	else:jobNamePrefix=sys.argv[2]

	os.system("qstat > qstat.out")
	try:
		with open('qstat.out','r')as qstatReadable:qstatStr=qstatReadable.read()
	except IOError:
		print'Error 07: File qstat.out was not found'
		exit()
	pattern='(\d+.ace)\s+'+jobNamePrefix
	jobNumbers=re.compile(pattern).findall(qstatStr)
	for job in jobNumbers:
		os.system("qdel %s"%job)

## Select tree
elif sys.argv[1]=="-s":
	FILE_EXT="*best.tre"
	BEST_LIKE=[]
	REPLICA=[]
	for LOG in glob.iglob(os.path.join(FILE_EXT)):
		NO=re.compile("(\S+).best.tre").findall(LOG)
		NO=NO[0]
		REPLICA+=[NO]
		with open(LOG,"r")as file:
			scores_dic={}
			text=file.read()
			scores=re.compile("!GarliScore\s+([^\]\s]+)").findall(text)
			if not scores:
				print("! ERROR: could not find any instance of final optimization in %s"%(LOG))
				exit()
			else:
				for key in range(0,len(scores)):
					scores_dic[key]=float(scores[key])
				best_rep=sorted(scores_dic,key=scores_dic.get,reverse=True)[0]
				best_like=scores_dic[best_rep]
				BEST_LIKE+=[best_like]
			print("Log file %s, best score %f"%(LOG,best_like))
	print("The best likelihood score is %f\nThe best tree(s) will be copied to ./BEST_LIKE/"%(max(BEST_LIKE)))
	INDEX=[i for i,n in enumerate(BEST_LIKE) if n==max(BEST_LIKE)]
	os.system("mkdir BEST_LIKE/")
	for n in INDEX:
		command="cp "+REPLICA[n]+"* BEST_LIKE/"
		os.system(command)


exit()
