sudoParallelGarli - Copyright (C) 2017 - Denis Jacob Machado
GNU General Public License version 3.0

sudoParallelGarli is desined to emulate Garli parallelization.

________________________________________________________________________________

sudoParallelGarli4bash.py (Python 3.+)

usage: sudoParallelGarli4bash.py [-h] -c CONFIGURATION -g GARLI
                                 [-p PROCESSORS] [-r REPLICAS] [-s] [-S START]
                                 [-V]
optional arguments:
  -h, --help            show this help message and exit
  -c CONFIGURATION, --configuration CONFIGURATION
                        Path to configuration file (defaulf = garli.conf)
  -g GARLI, --garli GARLI
                        Path to garli executable (defaulf = garli)
  -p PROCESSORS, --processors PROCESSORS
                        Maximum number of CPUs to use (default = 1)
  -r REPLICAS, --replicas REPLICAS
                        Specify a valid number of replicas greater than 1
                        (default = 1)
  -s, --select          Select best tree and quit (default = off)
  -S START, --Start START
                        Specify replicate start number (default = 1)
  -V, --version         print version number and exit

________________________________________________________________________________

sudoParallelGarli4pbs.py (Python 2.7)

sudoParallelGarli4pbs.py is designed to work in ACE
(http://www.ib.usp.br/grant/anfibios/researchHPC.html) or in very similar
environments using PBS. It requires garli.conf and garli.pbs (see examples/).

General command line:
	$ python sudoGarliParallel.py [-r; -k; -s] \
		[number of replicas; pbs prefix] \
		[number to start with (default = 1)]

Sudo-parallelization of Garli:
	$ python sudoGarliParallel.py -r <number of replicas>

Kill all replicas:
	$ python sudoGarliParallel.py -k <pbs prefix>

Select best tree in directory:
	$ python sudoGarliParallel.py -s

Exemple PBS script:

#!/bin/sh
#PBS -N GARLI_GTRG
#PBS -e GARLI_GTRG.1.0.err
#PBS -o GARLI_GTRG.1.0.out
#PBS -r n
#PBS -q high_mem
#PBS -l place=scatter
#PBS -l select=1:ncpus=1
# shm, sock, ssm, rdma, rdssm
FABRIC=rdma
CORES=$[ `cat $PBS_NODEFILE | wc -l` ]
NODES=$[ `uniq $PBS_NODEFILE | wc -l` ]
cd $PBS_O_WORKDIR
printf "Current time is: `date`\n";
printf "Current PBS work directory is: $PBS_O_WORKDIR\n";
printf "Current PBS queue is: $PBS_O_QUEUE\n";
printf "Current PBS job ID is: $PBS_JOBID\n";
printf "Current PBS job name is: $PBS_JOBNAME\n";
printf "PBS stdout log is: $PBS_O_WORKDIR/sgi_mpitest.err\n";
printf "PBS stderr log is: $PBS_O_WORKDIR/sgi_mpitest.log\n";
printf "Fabric interconnect selected is: $FABRIC\n";
printf "This jobs will run on $CORES processors.\n";
/apps/garli201serial garli.conf > STD.err 2> STD.out
printf "Job finished: `date`\n";
printf "Job walltime: `expr $TEND - $TBEGIN`\n";

Please feel free to modify the script according to your needs and feel free to
contact me by email in caes I can be of any help.
